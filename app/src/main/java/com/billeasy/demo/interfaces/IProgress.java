package com.billeasy.demo.interfaces;

public interface IProgress {

    void updateProgress(Integer message);
}
