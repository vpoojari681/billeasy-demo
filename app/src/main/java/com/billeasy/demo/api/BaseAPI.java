package com.billeasy.demo.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class BaseAPI {
    private static Retrofit instance = null;
    private static final String BASE_URL = "https://api.themoviedb.org/";

    BaseAPI() {
    }

    static Retrofit getInstance(){
        if (instance == null){
            instance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return instance;
    }
}
