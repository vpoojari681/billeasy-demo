package com.billeasy.demo.api;

import okhttp3.ResponseBody;
import retrofit2.Call;

import static com.billeasy.demo.utils.AppConstants.API_KEY;
import static com.billeasy.demo.utils.AppConstants.LANGUAGE;
import static com.billeasy.demo.utils.AppConstants.PAGES;

public class ApiHelper extends BaseAPI{
    private IApi iApi;

    public ApiHelper() {
        super();
        iApi = getInstance().create(IApi.class);
    }

    public Call<ResponseBody> getMovieList(){
        return iApi.getMovieList(API_KEY, LANGUAGE, PAGES);
    }
}
