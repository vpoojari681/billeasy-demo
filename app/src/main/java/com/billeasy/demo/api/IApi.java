package com.billeasy.demo.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IApi {

    String URL_GET_MOVIE = "3/movie/now_playing?";

    @POST(URL_GET_MOVIE)
    Call<ResponseBody> getMovieList(
            @Query("api_key") String API_KEY,
            @Query("language") String language,
            @Query("page") String page
    );

}
