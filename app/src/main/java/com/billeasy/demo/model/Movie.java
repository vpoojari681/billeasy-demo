package com.billeasy.demo.model;

import android.util.Log;

import com.billeasy.demo.api.ApiHelper;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Movie {

    public static final String MOVIE_TABLE = "Movie";
    public static final String COL_ID = "Id";
    public static final String COL_TITLE = "Title";
    public static final String COL_POSTER = "Poster";
    public static final String COL_BANNER = "Banner";
    public static final String COL_OVERVIEW = "Overview";
    public static final String COL_VOTE = "Vote";


    @SerializedName("results")
    @Expose
    private List<Result> results = null;

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public static void getMovie(final ICallBack iCallBack){
        ApiHelper apiHelper = new ApiHelper();
        apiHelper.getMovieList().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.body().string());
                                JSONArray jsonArrayCities = jsonObject.getJSONArray("results");
                                List<Result> cities = new ArrayList<>();
                                Gson gson = new Gson();
                                for (int i = 0; i < jsonArrayCities.length(); i++) {
                                    cities.add(gson.fromJson(jsonArrayCities.getJSONObject(i).toString(), Result.class));
                                }
                                iCallBack.onSuccess(cities);

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Error", "Error"+ t.getLocalizedMessage());
            }
        });
    }

    public interface ICallBack {
        void onSuccess(List<Result> cities);
        void getJSONObject(JSONObject jsonObject);
        void onError(String error);
    }

}
