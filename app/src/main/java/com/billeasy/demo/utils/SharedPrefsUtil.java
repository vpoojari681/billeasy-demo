package com.billeasy.demo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.billeasy.demo.utils.AppConstants.KEY_DB_UPLOAD;
import static com.billeasy.demo.utils.AppConstants.KEY_SYNC_SET;
import static com.billeasy.demo.utils.AppConstants.VALUE_DB_UPLOAD_NOT_DONE;
import static com.billeasy.demo.utils.AppConstants.VALUE_SYNC_SET_NO;

public class SharedPrefsUtil {



    public static int get(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int result = 0;
        if (key.equals(KEY_DB_UPLOAD)) {
            result = prefs.getInt(KEY_DB_UPLOAD, VALUE_DB_UPLOAD_NOT_DONE);
        } else if (key.equals(KEY_SYNC_SET)) {
            result = prefs.getInt(KEY_SYNC_SET, VALUE_SYNC_SET_NO);
        }
        return result;
    }


    public static void set(Context context, String key, int value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void setString(Context context, String key, String value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }
}
