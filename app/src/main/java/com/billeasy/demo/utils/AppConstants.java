package com.billeasy.demo.utils;

public class AppConstants {

    public static final String API_KEY = "bb20d498ee97820736e4cea8445f726e";
    public static final String LANGUAGE = "en-US";
    public static final String PAGES = "3";

    public static String KEY_DB_UPLOAD = "dbLoad";
    public static int VALUE_DB_UPLOAD_NOT_DONE = 0;
    public static int VALUE_DB_UPLOAD_DONE = 1;

    public static String KEY_LAST_UPDATE = "lastupdate";
    public static String VALUE_LAST_UPDATE = "3/4/2020";

    public final static String KEY_SYNC_SET = "sync_set";
    public final static int VALUE_SYNC_SET_NO = 0;
    public final static int VALUE_SYNC_SET_YES = 1;

}
