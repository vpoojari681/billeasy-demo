package com.billeasy.demo.sync;


import android.content.Context;
import android.util.Log;

import com.billeasy.demo.database.DatabaseHandler;
import com.billeasy.demo.interfaces.IProgress;
import com.billeasy.demo.model.Result;
import com.billeasy.demo.utils.AppConstants;
import com.billeasy.demo.utils.SharedPrefsUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SyncHelper {

    public static final String TAG = "SyncHelper";
    private DatabaseHandler databaseHandler;

    public SyncHelper(Context context) {
        databaseHandler = new DatabaseHandler(context);
    }

    public static boolean isInitialDBSetupDone(Context context) {
        boolean result;
        DatabaseHandler DatabaseHandler = new DatabaseHandler(context);
        int value = SharedPrefsUtil.get(context, AppConstants.KEY_DB_UPLOAD);
        if (value == AppConstants.VALUE_DB_UPLOAD_NOT_DONE) {
            if (DatabaseHandler.getResultCount() > 0) {
                SharedPrefsUtil.set(context, AppConstants.KEY_DB_UPLOAD, AppConstants.VALUE_DB_UPLOAD_DONE);
                result = true;
            } else {
                result = false;
            }
        } else {
            result = true;
        }
        return result;
    }

    /*
    * Saving data into database by parsing into list
     */
    public boolean saveDataLocally(JSONObject jsonObject, IProgress iProgress) {
        boolean result = false;
        boolean boolManufacture;

        if(iProgress!=null) iProgress.updateProgress(1);

        try {
            List<Result> movie = parseResult(jsonObject);
            boolManufacture = movie.size() > 0;
            databaseHandler.addOrUpdateMovie(movie);

            if(iProgress!=null) iProgress.updateProgress(2);
            if (boolManufacture ) result = true;

        } catch (Exception e) {
            Log.e(TAG, "Save Bulk():" + e.toString());
        }
        return result;
    }

    private List<Result> parseResult(JSONObject jsonObject) {
        List<Result> list = new ArrayList<>();
        try {
            if (jsonObject.has("results")) {
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                for (int i = 0; i < jsonArray.length(); i++) {
                    String string = jsonArray.getString(i);
                    Result result = Result.toResults(string);
                    if (result != null) {
                        list.add(result);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "parseResult():"+e.toString());
        }
        return list;
    }



}
