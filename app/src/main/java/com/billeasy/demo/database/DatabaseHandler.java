package com.billeasy.demo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.billeasy.demo.model.Result;

import java.util.ArrayList;
import java.util.List;

import static com.billeasy.demo.model.Movie.COL_BANNER;
import static com.billeasy.demo.model.Movie.COL_ID;
import static com.billeasy.demo.model.Movie.COL_OVERVIEW;
import static com.billeasy.demo.model.Movie.COL_POSTER;
import static com.billeasy.demo.model.Movie.COL_TITLE;
import static com.billeasy.demo.model.Movie.COL_VOTE;
import static com.billeasy.demo.model.Movie.MOVIE_TABLE;

public class DatabaseHandler extends SQLiteOpenHelper {

    private String TAG = "DatabaseHandler";
    private static final String DATABASE_NAME = "movie.db";
    private static final int DATABASE_VERSION = 2;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /*
    * Checking whether movie id exists in database. Return true if exists else false
    */
    public boolean exist(SQLiteDatabase db, String table, int key) {
        boolean result = false;
        String wherecondition = "ID=?";
        String[] args = {"" + key};
        Cursor cursor = null;
        try {
            cursor = db.query(table, null, wherecondition, args, null, null, null);
            if (cursor.moveToFirst()) {
                result = true;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    /*
    * Insert new data into database and
    * if data exists then table is updated
     */
    public synchronized void addOrUpdateMovie(List<Result> results) {
        synchronized (this) {
            SQLiteDatabase db = getWritableDatabase();
            try {
                db.beginTransaction();
                String table = MOVIE_TABLE;
                for (Result movie : results) {
                    ContentValues values = new ContentValues();
                    values.put(COL_ID, movie.getId());
                    values.put(COL_TITLE, movie.getOriginalTitle());
                    values.put(COL_POSTER, movie.getPosterPath());
                    values.put(COL_BANNER, movie.getBackdropPath());
                    values.put(COL_OVERVIEW, movie.getOverview());
                    values.put(COL_VOTE, movie.getVoteAverage());
                    if (exist(db, table, movie.getId())) {
                        String where = "ID=?";
                        String[] args = {"" + movie.getId()};
                        db.update(table, values, where, args);
                    } else {
                       db.insert(table, null, values);
                    }

                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                }
            }
        }
    }

    /*
    * Retrieving data from database
     */
    public List<Result> getMovieList() {
        List<Result> list = new ArrayList<Result>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "select distinct " + COL_ID + "," + COL_TITLE + "," + COL_POSTER + "," + COL_OVERVIEW + "," + COL_BANNER + "," + COL_VOTE
                +" from " + MOVIE_TABLE
                + " order by "+COL_TITLE;
        try {
            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Result movie = new Result();
                movie.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                movie.setTitle(cursor.getString(1));
                movie.setPosterPath(cursor.getString(2));
                movie.setOverview(cursor.getString(3));
                movie.setBackdropPath(cursor.getString(4));
                movie.setVoteCount(cursor.getInt(5));
                list.add(movie);
            }
        } catch (Exception e) {
            Log.e(TAG, "getResult()/ E:" + e.toString());
        }
        return list;
    }

    public int getResultCount() {
        int result = 0;
        SQLiteDatabase db = getReadableDatabase();
        try {
            String query = "SELECT COUNT() FROM " + MOVIE_TABLE;
            Cursor cursor = db.rawQuery(query, null);
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                result = cursor.getInt(0);
            }
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            db.close();
        }
        return result;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String createMovieDb = "CREATE TABLE " + MOVIE_TABLE + " ( "
                + COL_ID + " INTEGER PRIMARY KEY NOT NULL, "
                + COL_TITLE + " TEXT, "
                + COL_POSTER + " TEXT, "
                + COL_OVERVIEW + " TEXT, "
                + COL_BANNER + " TEXT, "
                + COL_VOTE + " INTEGER)";

        db.execSQL(createMovieDb);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
