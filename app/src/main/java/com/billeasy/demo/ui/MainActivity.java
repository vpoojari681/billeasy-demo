package com.billeasy.demo.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.billeasy.demo.R;
import com.billeasy.demo.api.ApiHelper;
import com.billeasy.demo.utils.AppConstants;
import com.billeasy.demo.utils.SharedPrefsUtil;
import com.billeasy.demo.sync.SyncHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.activity_main_txtProgress)
    TextView activityMainTxtProgress;
    Boolean bool = false;
    Context context;
    SyncHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = MainActivity.this;
        helper = new SyncHelper(context);
        startTimer();

    }

    private void startTimer() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                /*
                 * Getting row count from database
                 *
                 * If row count is greater than 0, return true else false
                 * If false fetch from API and save into database
                 */
                if (!SyncHelper.isInitialDBSetupDone(context)) {
                    fetchDataFromServer();
                } else {
                    goToNextScreen();
                }
            }
        }.start();
    }

    /*
     * Calling movie list API to fetch JSON and parsing it into list
     */
    private void fetchDataFromServer() {
        new ApiHelper().getMovieList().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(response.body().string());
                        if (helper.saveDataLocally(jsonObject, message -> {  //Storing data into database
                            switch (message) {
                                case 1:
                                    activityMainTxtProgress.setText(R.string.getting_data);
                                    break;
                                case 2:
                                    activityMainTxtProgress.setText(R.string.finalizing_data);
                                    break;
                            }
                        })) {
                            // Once data is saved locally, KEY_DB_UPLOAD is set to 1 and bool is set to true
                            bool = true;
                            goToNextScreen();
                            SharedPrefsUtil.set(context, AppConstants.KEY_DB_UPLOAD, AppConstants.VALUE_DB_UPLOAD_DONE);
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Error", "Error"+ t.getLocalizedMessage());
                activityMainTxtProgress.setText(R.string.connetc_to_internet);
            }
        });


    }


    private void goToNextScreen() {
        startActivity(new Intent(MainActivity.this, MovieActivity.class));
        finish();
    }


}

