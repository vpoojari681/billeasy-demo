package com.billeasy.demo.ui;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.billeasy.demo.R;
import com.billeasy.demo.model.Result;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity {

    @BindView(R.id.img_cover)
    ImageView imgCover;
    @BindView(R.id.img_poster)
    ImageView imgPoster;
    @BindView(R.id.text_title)
    TextView textTitle;
    @BindView(R.id.text_synopsis)
    TextView textSynopsis;
    Result movie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);

        if(getIntent() != null) {
            movie = (Result) getIntent().getSerializableExtra(getString(R.string.movie_data));
            populateData();
        }

    }

    private void populateData() {
        Glide.with(MovieDetailActivity.this).load(getString(R.string.image_path) + movie.getBackdropPath()).placeholder(R.drawable.movie_logo).into(imgCover);
        Glide.with(MovieDetailActivity.this).load(getString(R.string.image_path) + movie.getPosterPath()).placeholder(R.drawable.movie_logo).into(imgPoster);
        textTitle.setText(movie.getTitle());
        textSynopsis.setText(movie.getOverview());
    }
}
