package com.billeasy.demo.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.billeasy.demo.R;
import com.billeasy.demo.database.DatabaseHandler;
import com.billeasy.demo.model.Result;
import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieActivity extends AppCompatActivity {

    MovieAdapter movieAdapter;

    @BindView(R.id.actMovie_recyclerView)
    RecyclerView actMovieRecyclerView;

    List<Result> movieList;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        ButterKnife.bind(this);
        databaseHandler = new DatabaseHandler(MovieActivity.this);

        getSupportActionBar().setTitle(R.string.now_playing);
        movieList = databaseHandler.getMovieList();
        movieAdapter = new MovieAdapter(MovieActivity.this, movieList);
        actMovieRecyclerView.setLayoutManager(new GridLayoutManager(MovieActivity.this, 2));
        actMovieRecyclerView.setAdapter(movieAdapter);
        //serverCall();
    }


    //Try this code for shimmer effects like facebook
    //It just retrieves data through API

   /* private void serverCall() {
        Movie.getMovie(new Movie.ICallBack() {
            @Override
            public void onSuccess(List<Result> cities) {
                movieList.addAll(cities);
                movieAdapter.notifyDataSetChanged();

                shimmerViewContainer.stopShimmerAnimation();
                shimmerViewContainer.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {

            }

            @Override
            public void getJSONObject(JSONObject jsonObject){

            }
        });
    }*/

    @Override
    public void onResume() {
        super.onResume();
       // shimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        //shimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    public static class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

        private List<Result> movieList;
        private Activity movieActivity;

        MovieAdapter(MovieActivity movieActivity, List<Result> movieList) {
            this.movieActivity = movieActivity;
            this.movieList = movieList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(movieActivity).inflate(R.layout.item_movie, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Glide.with(movieActivity).load(movieActivity.getString(R.string.image_path) + movieList.get(position).getPosterPath()).placeholder(R.drawable.preview).into(holder.itemMovieImage);
            holder.itemMovieCard.setOnClickListener(v->{
                Intent intent = new Intent(movieActivity, MovieDetailActivity.class);
                intent.putExtra(movieActivity.getString(R.string.movie_data), (Serializable) movieList.get(position));
                movieActivity.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return movieList.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            ImageView itemMovieImage;
            CardView itemMovieCard;

            ViewHolder(@NonNull View itemView) {
                super(itemView);

                itemMovieImage = itemView.findViewById(R.id.item_movieImage);
                itemMovieCard = itemView.findViewById(R.id.item_movieCard);
            }
        }
    }
}
